{
    "id": "ac6865e4-542e-40df-a54f-665ad3dfe134",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_wall",
    "eventList": [
        {
            "id": "fa1e6c8b-8936-4174-9e39-85ef0a878b8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ac6865e4-542e-40df-a54f-665ad3dfe134"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "443302c7-85e0-4be5-ae8b-2ee280e254f8",
    "visible": true
}