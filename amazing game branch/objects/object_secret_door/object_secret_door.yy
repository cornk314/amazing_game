{
    "id": "18113fd5-8037-4529-9016-c3bd02527bb3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_secret_door",
    "eventList": [
        {
            "id": "ad6031bc-e86d-4afc-ab02-6876334d83fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "18113fd5-8037-4529-9016-c3bd02527bb3"
        },
        {
            "id": "1a9f343d-48fb-45ee-82f6-aba1729cb3f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "fb7204cc-213d-4655-970d-7a693e434646",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "18113fd5-8037-4529-9016-c3bd02527bb3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "971fa405-f7ee-47f9-9996-a8d09248678d",
    "visible": false
}