{
    "id": "2c71eb9f-31f7-44b9-b433-b70f139677f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_clear_wall_block",
    "eventList": [
        {
            "id": "70e3233a-59e7-46f1-8b4e-018983c9ec7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2c71eb9f-31f7-44b9-b433-b70f139677f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "443302c7-85e0-4be5-ae8b-2ee280e254f8",
    "visible": true
}