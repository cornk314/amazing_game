{
    "id": "82aa823b-5676-4127-89b0-9fb8e12994ef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_door",
    "eventList": [
        {
            "id": "79cfeee3-4dda-405d-8f29-0640eaac0add",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82aa823b-5676-4127-89b0-9fb8e12994ef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fa71e19c-7019-4987-b1b8-483f8192379b",
    "visible": true
}