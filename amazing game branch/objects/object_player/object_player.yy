{
    "id": "fb7204cc-213d-4655-970d-7a693e434646",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player",
    "eventList": [
        {
            "id": "563e9d63-2ef6-4567-b84c-389e8b6472aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb7204cc-213d-4655-970d-7a693e434646"
        },
        {
            "id": "c9aa7745-57c1-4302-b82a-4cd9a739c4b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "fb7204cc-213d-4655-970d-7a693e434646"
        },
        {
            "id": "1416c244-216c-418e-ab73-705add5ec7c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 5,
            "m_owner": "fb7204cc-213d-4655-970d-7a693e434646"
        },
        {
            "id": "74004b29-5b11-4341-a9b8-097c840cd740",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "fb7204cc-213d-4655-970d-7a693e434646"
        },
        {
            "id": "cb8e679b-86b7-4348-84ac-ddec407767eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "fb7204cc-213d-4655-970d-7a693e434646"
        },
        {
            "id": "25d0dc31-9f95-4a1d-9b28-8be76bb15915",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "fb7204cc-213d-4655-970d-7a693e434646"
        },
        {
            "id": "87e6cc76-cc0e-44bc-8dfb-209fa1650b84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "ac6865e4-542e-40df-a54f-665ad3dfe134",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fb7204cc-213d-4655-970d-7a693e434646"
        },
        {
            "id": "e563b83c-0e48-40a3-984d-1b7c12458684",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "82aa823b-5676-4127-89b0-9fb8e12994ef",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fb7204cc-213d-4655-970d-7a693e434646"
        },
        {
            "id": "03f5e3f3-f9d2-4a3f-96f3-f69226cb54b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 33,
            "eventtype": 9,
            "m_owner": "fb7204cc-213d-4655-970d-7a693e434646"
        },
        {
            "id": "a038500c-4da8-4694-a80c-8aacbd505604",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 34,
            "eventtype": 9,
            "m_owner": "fb7204cc-213d-4655-970d-7a693e434646"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "50ccab22-d96a-486f-a633-35c2fac4d0f4",
    "visible": true
}