/// @DnDAction : YoYo Games.Movement.Set_Direction_Fixed
/// @DnDVersion : 1.1
/// @DnDHash : 561F3091
/// @DnDArgument : "direction" "270"
direction = 270;

/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 0D01D207
/// @DnDArgument : "speed" "4"
speed = 4;

/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 1C87919D
image_speed = 1;

/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 70288775
/// @DnDArgument : "imageind" "image_index"
/// @DnDArgument : "spriteind" "sprite_playerdown"
/// @DnDSaveInfo : "spriteind" "50ccab22-d96a-486f-a633-35c2fac4d0f4"
sprite_index = sprite_playerdown;
image_index = image_index;

/// @DnDAction : YoYo Games.Instances.Sprite_Scale
/// @DnDVersion : 1
/// @DnDHash : 5A0E3D3D
image_xscale = 1;
image_yscale = 1;