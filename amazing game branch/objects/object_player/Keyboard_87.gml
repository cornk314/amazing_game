/// @DnDAction : YoYo Games.Movement.Set_Direction_Fixed
/// @DnDVersion : 1.1
/// @DnDHash : 561F3091
/// @DnDArgument : "direction" "90"
direction = 90;

/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 0D01D207
/// @DnDArgument : "speed" "4"
speed = 4;

/// @DnDAction : YoYo Games.Instances.Sprite_Animation_Speed
/// @DnDVersion : 1
/// @DnDHash : 1C87919D
image_speed = 1;

/// @DnDAction : YoYo Games.Instances.Set_Sprite
/// @DnDVersion : 1
/// @DnDHash : 70288775
/// @DnDArgument : "imageind" "image_index"
/// @DnDArgument : "spriteind" "sprite_playerup"
/// @DnDSaveInfo : "spriteind" "523034a1-bb59-4f82-973f-c812726b014a"
sprite_index = sprite_playerup;
image_index = image_index;