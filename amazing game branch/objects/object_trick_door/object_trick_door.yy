{
    "id": "e6f9d4f3-5a54-4575-b266-484e92deac5e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_trick_door",
    "eventList": [
        {
            "id": "9edf3751-6428-4cd6-959a-dfe00ca878b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e6f9d4f3-5a54-4575-b266-484e92deac5e"
        },
        {
            "id": "b0265733-691f-4bbf-beaa-0b8d505ec125",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "fb7204cc-213d-4655-970d-7a693e434646",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e6f9d4f3-5a54-4575-b266-484e92deac5e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fa71e19c-7019-4987-b1b8-483f8192379b",
    "visible": true
}