{
    "id": "971fa405-f7ee-47f9-9996-a8d09248678d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_secret_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 27,
    "bbox_right": 28,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "857ef44e-2be1-4cf8-a0df-cc1ccaff03c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "971fa405-f7ee-47f9-9996-a8d09248678d",
            "compositeImage": {
                "id": "e89c314b-dae9-4c0b-9d88-5241e91cfb8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "857ef44e-2be1-4cf8-a0df-cc1ccaff03c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3edc62f-198e-427d-a28a-5eee7eda7aac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "857ef44e-2be1-4cf8-a0df-cc1ccaff03c9",
                    "LayerId": "297ff55e-bff9-44a1-9ea9-8f7cdaafd2ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "297ff55e-bff9-44a1-9ea9-8f7cdaafd2ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "971fa405-f7ee-47f9-9996-a8d09248678d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}