{
    "id": "50ccab22-d96a-486f-a633-35c2fac4d0f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_playerdown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4712450-6f98-497e-a8cb-e25dc03e6055",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50ccab22-d96a-486f-a633-35c2fac4d0f4",
            "compositeImage": {
                "id": "a3e0d9ee-a6b1-417d-b02e-9899a103690e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4712450-6f98-497e-a8cb-e25dc03e6055",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d840732b-86ce-4c82-b1fb-5ca501e1b03b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4712450-6f98-497e-a8cb-e25dc03e6055",
                    "LayerId": "f39e3b36-cce7-4e66-bf8a-3f1430d1452c"
                }
            ]
        },
        {
            "id": "b52d9f0a-650e-4db0-bad8-dfd8aaa3add5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50ccab22-d96a-486f-a633-35c2fac4d0f4",
            "compositeImage": {
                "id": "0a99391b-8e93-4669-b2b4-a818443c7f9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b52d9f0a-650e-4db0-bad8-dfd8aaa3add5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f86ff66b-3081-4a02-87ae-381860ca2d90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b52d9f0a-650e-4db0-bad8-dfd8aaa3add5",
                    "LayerId": "f39e3b36-cce7-4e66-bf8a-3f1430d1452c"
                }
            ]
        },
        {
            "id": "b9a62c57-40a1-4d3b-b893-76366fa05153",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50ccab22-d96a-486f-a633-35c2fac4d0f4",
            "compositeImage": {
                "id": "a05d84ed-4c80-41f1-9dfc-c72843fe636f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9a62c57-40a1-4d3b-b893-76366fa05153",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d254a1a-8aae-44c6-85c5-63167b5e4cc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9a62c57-40a1-4d3b-b893-76366fa05153",
                    "LayerId": "f39e3b36-cce7-4e66-bf8a-3f1430d1452c"
                }
            ]
        },
        {
            "id": "96591e20-a043-4aef-bcbc-a6db6e7d57bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50ccab22-d96a-486f-a633-35c2fac4d0f4",
            "compositeImage": {
                "id": "22edaa44-4a9a-451f-8ae0-1c4089dcd6cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96591e20-a043-4aef-bcbc-a6db6e7d57bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e93954bc-8dfe-4333-9d0a-134c39ebadce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96591e20-a043-4aef-bcbc-a6db6e7d57bc",
                    "LayerId": "f39e3b36-cce7-4e66-bf8a-3f1430d1452c"
                }
            ]
        },
        {
            "id": "dfc8a83e-437d-4b56-91e2-1bb9528c6dda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50ccab22-d96a-486f-a633-35c2fac4d0f4",
            "compositeImage": {
                "id": "6892d46d-a749-4cdf-88e4-af4f4abf6a9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfc8a83e-437d-4b56-91e2-1bb9528c6dda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cab3459-feef-4329-a986-d5dcb02cb78e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfc8a83e-437d-4b56-91e2-1bb9528c6dda",
                    "LayerId": "f39e3b36-cce7-4e66-bf8a-3f1430d1452c"
                }
            ]
        },
        {
            "id": "c9abf89d-a9d7-48d4-b3da-1c86870eaf5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50ccab22-d96a-486f-a633-35c2fac4d0f4",
            "compositeImage": {
                "id": "06710387-39c9-4504-b3b1-ab4bbf19ca72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9abf89d-a9d7-48d4-b3da-1c86870eaf5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0cc1553-f82a-4e7b-83cf-083d9b716059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9abf89d-a9d7-48d4-b3da-1c86870eaf5c",
                    "LayerId": "f39e3b36-cce7-4e66-bf8a-3f1430d1452c"
                }
            ]
        },
        {
            "id": "dcf0948e-896d-4af9-9f6c-61a65e9c7d40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50ccab22-d96a-486f-a633-35c2fac4d0f4",
            "compositeImage": {
                "id": "b1dd43a7-4015-4f26-98d8-bbfe7fb72202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcf0948e-896d-4af9-9f6c-61a65e9c7d40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "245417a5-a7a2-4a9e-814e-9ce20075721e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcf0948e-896d-4af9-9f6c-61a65e9c7d40",
                    "LayerId": "f39e3b36-cce7-4e66-bf8a-3f1430d1452c"
                }
            ]
        },
        {
            "id": "a742fc37-268e-4f80-9523-65377f0dcdd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50ccab22-d96a-486f-a633-35c2fac4d0f4",
            "compositeImage": {
                "id": "6833571a-1e8b-4e52-ac92-c9e9e3fddd6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a742fc37-268e-4f80-9523-65377f0dcdd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44c1318e-5afd-41a3-880b-f3291723e28d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a742fc37-268e-4f80-9523-65377f0dcdd0",
                    "LayerId": "f39e3b36-cce7-4e66-bf8a-3f1430d1452c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f39e3b36-cce7-4e66-bf8a-3f1430d1452c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50ccab22-d96a-486f-a633-35c2fac4d0f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}