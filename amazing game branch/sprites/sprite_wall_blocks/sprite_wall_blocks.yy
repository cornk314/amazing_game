{
    "id": "443302c7-85e0-4be5-ae8b-2ee280e254f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_wall_blocks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97034e9c-2cf8-4d9e-bf0f-83c643a17565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "443302c7-85e0-4be5-ae8b-2ee280e254f8",
            "compositeImage": {
                "id": "3e635bc4-73ad-4e4f-b633-ea80b354f360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97034e9c-2cf8-4d9e-bf0f-83c643a17565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac4d83e1-598e-4c85-b72a-484d9ef3be84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97034e9c-2cf8-4d9e-bf0f-83c643a17565",
                    "LayerId": "d0438c3e-f19a-4fe8-8969-9a8cd2a8f593"
                }
            ]
        },
        {
            "id": "a4b13aaa-8f1f-48eb-8742-d0e5c66bd5f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "443302c7-85e0-4be5-ae8b-2ee280e254f8",
            "compositeImage": {
                "id": "f26e1e5c-ce84-4e8e-a91e-3e3cbe301d3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4b13aaa-8f1f-48eb-8742-d0e5c66bd5f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92733f4d-6a06-48ee-aedb-c9e7ce710e6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4b13aaa-8f1f-48eb-8742-d0e5c66bd5f2",
                    "LayerId": "d0438c3e-f19a-4fe8-8969-9a8cd2a8f593"
                }
            ]
        },
        {
            "id": "e6baf8aa-a8de-4745-bf4b-31418e8a754d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "443302c7-85e0-4be5-ae8b-2ee280e254f8",
            "compositeImage": {
                "id": "4e497baa-d616-4e1d-b078-f342ced7dc04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6baf8aa-a8de-4745-bf4b-31418e8a754d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bea8eeff-e086-47d0-a088-dae791f8438a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6baf8aa-a8de-4745-bf4b-31418e8a754d",
                    "LayerId": "d0438c3e-f19a-4fe8-8969-9a8cd2a8f593"
                }
            ]
        },
        {
            "id": "8e57c63b-1523-4bfe-b2fb-90705eae41a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "443302c7-85e0-4be5-ae8b-2ee280e254f8",
            "compositeImage": {
                "id": "111b24b6-4f26-4a73-88ae-478cbcf05c26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e57c63b-1523-4bfe-b2fb-90705eae41a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deacb913-9004-4113-9311-ffd100965461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e57c63b-1523-4bfe-b2fb-90705eae41a9",
                    "LayerId": "d0438c3e-f19a-4fe8-8969-9a8cd2a8f593"
                }
            ]
        },
        {
            "id": "af9c177f-ae67-45b2-a8ad-447783d04972",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "443302c7-85e0-4be5-ae8b-2ee280e254f8",
            "compositeImage": {
                "id": "061938f1-54b3-49cd-ac2c-7e5220dee459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af9c177f-ae67-45b2-a8ad-447783d04972",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c7940e1-6d45-48bd-9d4a-9b4a742e49dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af9c177f-ae67-45b2-a8ad-447783d04972",
                    "LayerId": "d0438c3e-f19a-4fe8-8969-9a8cd2a8f593"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d0438c3e-f19a-4fe8-8969-9a8cd2a8f593",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "443302c7-85e0-4be5-ae8b-2ee280e254f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}