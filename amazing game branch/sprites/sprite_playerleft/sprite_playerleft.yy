{
    "id": "4f98fa8d-9de7-4dba-b7e1-a5d01c7af85b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_playerleft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06855cbf-fab0-41d4-8436-a86e99dc1d24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f98fa8d-9de7-4dba-b7e1-a5d01c7af85b",
            "compositeImage": {
                "id": "cf7e8fa8-0a8c-487a-8202-d5c3d0dc9cea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06855cbf-fab0-41d4-8436-a86e99dc1d24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "279f0ab9-23cf-4cad-9622-508a443b8654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06855cbf-fab0-41d4-8436-a86e99dc1d24",
                    "LayerId": "a19c6a24-07ab-4ff4-8ff8-2ffb1df50a0a"
                }
            ]
        },
        {
            "id": "81172e35-ea35-4c1d-8681-0ea8ceff4c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f98fa8d-9de7-4dba-b7e1-a5d01c7af85b",
            "compositeImage": {
                "id": "5db274fc-cf7c-4024-8cc4-cb474a717196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81172e35-ea35-4c1d-8681-0ea8ceff4c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e82c22d-f0b5-4e07-bf30-4d4fc2d8fee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81172e35-ea35-4c1d-8681-0ea8ceff4c01",
                    "LayerId": "a19c6a24-07ab-4ff4-8ff8-2ffb1df50a0a"
                }
            ]
        },
        {
            "id": "56766fbe-039e-4a40-bca5-f0d8e5044e35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f98fa8d-9de7-4dba-b7e1-a5d01c7af85b",
            "compositeImage": {
                "id": "31eb4f3c-7656-4a57-9fbe-ad49a726dd97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56766fbe-039e-4a40-bca5-f0d8e5044e35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1f909de-b860-47b6-a164-6c713155ec0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56766fbe-039e-4a40-bca5-f0d8e5044e35",
                    "LayerId": "a19c6a24-07ab-4ff4-8ff8-2ffb1df50a0a"
                }
            ]
        },
        {
            "id": "b1fe9711-dc2b-47d0-b397-a354b96b753c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f98fa8d-9de7-4dba-b7e1-a5d01c7af85b",
            "compositeImage": {
                "id": "dffb306c-94cd-4812-9bc0-c8bfea5745cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1fe9711-dc2b-47d0-b397-a354b96b753c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff0a68f8-d4a9-44a1-8078-bf52e0bbbd14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1fe9711-dc2b-47d0-b397-a354b96b753c",
                    "LayerId": "a19c6a24-07ab-4ff4-8ff8-2ffb1df50a0a"
                }
            ]
        },
        {
            "id": "01b9ba9f-cc3e-4c70-96a5-f5a4d336553d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f98fa8d-9de7-4dba-b7e1-a5d01c7af85b",
            "compositeImage": {
                "id": "56a3e0f5-9ae7-4aac-bef3-4c271753c70e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01b9ba9f-cc3e-4c70-96a5-f5a4d336553d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95dfa24a-132b-49c3-9abe-3adf3829977b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01b9ba9f-cc3e-4c70-96a5-f5a4d336553d",
                    "LayerId": "a19c6a24-07ab-4ff4-8ff8-2ffb1df50a0a"
                }
            ]
        },
        {
            "id": "bd318418-27bb-412b-81e7-5b54938f2dc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f98fa8d-9de7-4dba-b7e1-a5d01c7af85b",
            "compositeImage": {
                "id": "964810e2-d27f-46a5-a5c4-08aba637ad9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd318418-27bb-412b-81e7-5b54938f2dc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76f5c417-81e6-49c2-b0a4-82124d2365d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd318418-27bb-412b-81e7-5b54938f2dc1",
                    "LayerId": "a19c6a24-07ab-4ff4-8ff8-2ffb1df50a0a"
                }
            ]
        },
        {
            "id": "2210afc6-782b-4bb0-b3bf-436a70ae2692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f98fa8d-9de7-4dba-b7e1-a5d01c7af85b",
            "compositeImage": {
                "id": "ad6805ff-bc28-4fe8-ae40-6a32e721ccac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2210afc6-782b-4bb0-b3bf-436a70ae2692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80d132bc-affc-4eac-9454-392dede8c0a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2210afc6-782b-4bb0-b3bf-436a70ae2692",
                    "LayerId": "a19c6a24-07ab-4ff4-8ff8-2ffb1df50a0a"
                }
            ]
        },
        {
            "id": "c8425531-b6c6-4aeb-b9b4-24f1733e850c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f98fa8d-9de7-4dba-b7e1-a5d01c7af85b",
            "compositeImage": {
                "id": "e22ed7fe-2ef2-4fc0-9ab6-97fda7d8a2b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8425531-b6c6-4aeb-b9b4-24f1733e850c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "600aacef-4cba-4fd3-894d-65c5c55f99ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8425531-b6c6-4aeb-b9b4-24f1733e850c",
                    "LayerId": "a19c6a24-07ab-4ff4-8ff8-2ffb1df50a0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a19c6a24-07ab-4ff4-8ff8-2ffb1df50a0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f98fa8d-9de7-4dba-b7e1-a5d01c7af85b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}