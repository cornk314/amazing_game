{
    "id": "523034a1-bb59-4f82-973f-c812726b014a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_playerup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9205fbe6-58e6-4e7b-9aae-61e4aee4b32d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "523034a1-bb59-4f82-973f-c812726b014a",
            "compositeImage": {
                "id": "314ab229-d7e5-4c32-bd82-38d28d73d809",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9205fbe6-58e6-4e7b-9aae-61e4aee4b32d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91fcd13d-a16c-4178-95e6-3587b66d0a3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9205fbe6-58e6-4e7b-9aae-61e4aee4b32d",
                    "LayerId": "cbb3b655-bc5f-49b7-be15-32399e696d14"
                }
            ]
        },
        {
            "id": "f3a90ee1-d19f-4ecc-8d4d-41dc20fc94af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "523034a1-bb59-4f82-973f-c812726b014a",
            "compositeImage": {
                "id": "768f8e6d-81d5-4c9a-86aa-b35131a1298d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3a90ee1-d19f-4ecc-8d4d-41dc20fc94af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2bbacae-b30e-461a-900e-26d842131ad2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3a90ee1-d19f-4ecc-8d4d-41dc20fc94af",
                    "LayerId": "cbb3b655-bc5f-49b7-be15-32399e696d14"
                }
            ]
        },
        {
            "id": "49b18008-53cb-40e9-949a-e04982d28578",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "523034a1-bb59-4f82-973f-c812726b014a",
            "compositeImage": {
                "id": "8fd2b4ac-6fed-4a5b-92c3-810fc00332de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49b18008-53cb-40e9-949a-e04982d28578",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "851d1866-b6d2-4115-a3f7-546b14d2d54c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49b18008-53cb-40e9-949a-e04982d28578",
                    "LayerId": "cbb3b655-bc5f-49b7-be15-32399e696d14"
                }
            ]
        },
        {
            "id": "ba64bce8-6845-4534-9507-85ed6a75920e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "523034a1-bb59-4f82-973f-c812726b014a",
            "compositeImage": {
                "id": "d26210c5-dba0-495e-b75c-eab93276c3e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba64bce8-6845-4534-9507-85ed6a75920e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5f5cce2-9915-4b3b-8ba0-b3886ef2bf47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba64bce8-6845-4534-9507-85ed6a75920e",
                    "LayerId": "cbb3b655-bc5f-49b7-be15-32399e696d14"
                }
            ]
        },
        {
            "id": "06509c49-ef6a-4603-b175-1b22fcca9d93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "523034a1-bb59-4f82-973f-c812726b014a",
            "compositeImage": {
                "id": "5c6021c0-c648-4441-af75-5b157ba44c50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06509c49-ef6a-4603-b175-1b22fcca9d93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0943be5f-9238-48ae-b259-545fa6226f50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06509c49-ef6a-4603-b175-1b22fcca9d93",
                    "LayerId": "cbb3b655-bc5f-49b7-be15-32399e696d14"
                }
            ]
        },
        {
            "id": "623c6545-0b51-41a7-b68b-412453eb1750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "523034a1-bb59-4f82-973f-c812726b014a",
            "compositeImage": {
                "id": "40368d13-f7b9-4238-9b37-72c47d38cc53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "623c6545-0b51-41a7-b68b-412453eb1750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6290460e-e557-4399-af8f-678248027e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "623c6545-0b51-41a7-b68b-412453eb1750",
                    "LayerId": "cbb3b655-bc5f-49b7-be15-32399e696d14"
                }
            ]
        },
        {
            "id": "a5064d2a-924f-4a71-98b9-5e9dff4fe724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "523034a1-bb59-4f82-973f-c812726b014a",
            "compositeImage": {
                "id": "fc051c89-53de-4ca5-8ad9-6ab75d2a4e3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5064d2a-924f-4a71-98b9-5e9dff4fe724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50b031a2-1228-4dcc-8184-e6565008db74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5064d2a-924f-4a71-98b9-5e9dff4fe724",
                    "LayerId": "cbb3b655-bc5f-49b7-be15-32399e696d14"
                }
            ]
        },
        {
            "id": "0e839888-e906-4a6d-8937-7e1f9b4eb70a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "523034a1-bb59-4f82-973f-c812726b014a",
            "compositeImage": {
                "id": "e4901d1d-cffa-487f-ae56-d59f9d80974c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e839888-e906-4a6d-8937-7e1f9b4eb70a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18f6ab5a-8607-47dc-834b-9fb5dc1423cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e839888-e906-4a6d-8937-7e1f9b4eb70a",
                    "LayerId": "cbb3b655-bc5f-49b7-be15-32399e696d14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cbb3b655-bc5f-49b7-be15-32399e696d14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "523034a1-bb59-4f82-973f-c812726b014a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}