{
    "id": "fa71e19c-7019-4987-b1b8-483f8192379b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cd315fc-5255-461d-b44e-6cd5b51c5770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa71e19c-7019-4987-b1b8-483f8192379b",
            "compositeImage": {
                "id": "e505df48-47f6-4b04-a42d-5c6d4166921f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd315fc-5255-461d-b44e-6cd5b51c5770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c566a912-d712-4878-a288-092cc22bc393",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd315fc-5255-461d-b44e-6cd5b51c5770",
                    "LayerId": "05ee156c-bff9-418c-9081-72fa42e31d50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "05ee156c-bff9-418c-9081-72fa42e31d50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa71e19c-7019-4987-b1b8-483f8192379b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}